#!/usr/bin/env python
# coding=utf-8
"""Functions for geotiff download."""

import os

import config
import ee
import jinja2
import webapp2
import datetime
from google.appengine.api import urlfetch
import socket
import json
import math
#from satellites import *
from glwfunctions import *


def applyAlgorithm(_img, _sat, _alg, _satJson, _algStr):
    img = ee.Image(_img)
    # den Expression string holen für den sat und den algo
    expr = getExpressionString(_sat, _alg, _satJson, _algStr)
    # expression dict holen (keys sind Variablennamen im expresionstring, values sind ee.Image mit einem Band)
    dict = getExpressionDict(_sat, _alg, _satJson, img)

    print("expr " + expr)
    print("exprdict " + str(dict))
    # expression für den Algorithmus auführen
    # Ergebnis ist wieder ein ee.Image
    algImg = img.expression(expr, dict)
    return algImg

def evalCallback(success, failure):
    print(success)
    print("failure: " + failure)

def getGeotiffDownloadinRegion(templateValues, currentSatJson, algStr, colorImage):
    dateStr = templateValues["dateStr"]
    sat = templateValues["satellite"]
    alg = templateValues["algorithm"]
    
    nelat = float(templateValues["nelat"])
    nelng = float(templateValues["nelng"])
    swlat = float(templateValues["swlat"])
    swlng = float(templateValues["swlng"])
	
    regionList = [[nelng, nelat], [nelng, swlat], [swlng, swlat], [swlng, nelat]]
    print("RegionList: ")
    print(regionList)
    region = ee.Geometry.Polygon(regionList)
    #regionStr ='[[], [], [], []]'
	
    desiredDate = ee.Date.parse("yyyy-MM-dd", dateStr)
    halfDiff = (currentSatJson["repeatInterval"] * 24) / 2;
    dateRange = ee.DateRange(desiredDate.advance(-halfDiff, 'hour'), desiredDate.advance(halfDiff, 'hour'));
    # richtige imagecollection erzeugen, gleich filtern
    imageColl=ee.ImageCollection(currentSatJson["bandDataImageString"]).filterDate(dateRange).filterBounds(region).mosaic()
	
    url = ""
    if alg != "RGB" :
        image = applyAlgorithm(imageColl, sat, alg, currentSatJson, algStr)
        if colorImage == True:
            mini = float(templateValues["minStr"])
            maxi = float(templateValues["maxStr"])
            normalizedAlg = image.subtract(mini).divide(maxi - mini)
            paletteList = ['000000', '0000FF', '00FFFF', '00FF00', 'FFFF00', 'FF0000']
            colorfulImg = normalizedAlg.visualize(min=0, max=1, palette=paletteList)
            
            if sat != "Modis":
                # die Wolkenmaske und Wassermaske holen (ee.Image)
                cloudMask = getCloudMask(dateRange, sat, currentSatJson)
                waterMask = getWaterMask(dateRange, sat)
                # colorfulImg = colorfulImg.updateMask(cloudMask)

                # ein graues Bild erzeugen (generisch, also egal welche Grösse)
                grayImage = ee.Image.constant([128, 128, 128]);
                # überall wo wolken sind, grau malen
                colorfulImg = grayImage.where(cloudMask, colorfulImg)
                # überall wo kein wasser ist, maskieren (also transparent machen)
                colorfulImg = colorfulImg.updateMask(waterMask)
            # getThumbUrl liefert ein jpg oder png vom Farbbild
            url = colorfulImg.getThumbURL({'region': str(regionList), 'name': 'testDownload', 'dimensions': 1920, 'format': 'png' })
        else:
            url = image.getDownloadURL({'region': str(regionList), 'name': 'testDownload', 'scale': 10 })
    else :
        image = ee.Image(imageColl)
        url = image.getDownloadURL({'region': str(regionList), 'name': 'testDownload', 'scale': 10 })

    

    print("dateStr: " + dateStr)
    print("satellite " + sat)
    print("algorithm " + alg)

    #img = ee.Image(image.mosaic());
    #print("bandnames")
    #print(image.bandNames().getInfo())
    #url = colorfulImg.getDownloadURL({'region': str(regionList), 'name': 'testDownload', 'scale': 10 })
    #url = image.getThumbURL({'region': str(regionList), 'name': 'testDownload', 'scale': 10 })
	
    return url
	
