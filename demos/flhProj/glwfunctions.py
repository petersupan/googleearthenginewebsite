#!/usr/bin/env python
# coding=utf-8
"""Functions of lakewatch. This is just the beginning for modularization. More to come."""

import os

import config
import ee
import jinja2
import webapp2
import datetime
from google.appengine.api import urlfetch
import socket

#from satellites import *  # satellite config


# Wassermaske zurückgeben.
# Eingabe ist 
# 	eine ee.DateRange und 
#	ein string, welcher Satellit (Der muss gleich sein)
# Returnwert ist ein ee.Image
# zur Zeit wird immer die (statische, relativ hoch aufgelöste) Watermask vom Global Forest watch verwendet
# Es hat aber jeder Satellit eigene Wassermasken, die grad bei schlecht aufgelösten und bisschen verschobenen
# wie Sentinel 3 besser wären um nicht immer einen Streifen Land rein zu kriegen
def getWaterMask(dateRange, sat):
    # if "Sentinel3" != sat:
    hansenImage = ee.Image('UMD/hansen/global_forest_change_2013')
    data = hansenImage.select('datamask')
    waterMask = data.neq(1)
    # else:
    #     qaImage = ee.ImageCollection(cloudMaskImageStrings[sat]).filterDate(dateRange).mosaic().select('quality_flags')
    #     # inland water
    #     waterMask = qaImage.bitwiseAnd(0x20000000).neq(0)
    #     # sea water
    #     waterMask = waterMask.bitwiseOr(qaImage.bitwiseAnd(0x80000000).eq(0))

    return waterMask


# Wolkenmaske zurückgeben
# Eingabe ist 
# 	eine ee.DateRange und 
#	ein string, welcher Satellit (Der muss gleich sein)
# Returnwert ist ein ee.Image
# Die Cloudmaske wird aus den QA Flags geholt, ist aber sehr aggressiv (also nur was wirklich total vollbewölkt ist, ist drinnen)
# Andràs meint, es wär besser, wir berechnen unsere eigene, um keine Fehler drin zu haben
def getCloudMask(dateRange, currentSat, satJson):
    cmIStr = satJson["cloudMaskImageString"]
    # cmIStr = cloudMaskImageStrings[currentSat]
    print("cloudmask image for sat" + currentSat + " is " + cmIStr)
    imageForCloud = ee.ImageCollection(cmIStr).filterDate(dateRange).mosaic()
    cloudMask = None;

    if currentSat == "Sentinel2":
        cloudMask = imageForCloud.select('QA60').bitwiseAnd(0xc00).eq(0)
    elif currentSat == "Sentinel3":
        cloudMask = imageForCloud.select('quality_flags').bitwiseAnd(0x8000000).eq(0)
    else:
        cloudMask = imageForCloud.select('BQA').bitwiseAnd(0x1000).eq(0)
    return cloudMask


# den Expression String für den aktuellen Algorithmus auf dem aktuellen Satelliten bitte
# Eingabe ist 
#	ein string (welcher Satellit)
#	ein string (welcher Algorithmus)
#   ein dict vom aktuellen satellit
#   der default algstr des aktuellen algorithmus
# Return ist ein string
def getExpressionString(sat, alg, satJson, algStr):
    exprString = algStr;
    # hat der Satellit ein override (wegen unguenstigen Baendern)?
    if ("expressionStringOverride" in satJson["algorithms"][alg]):
        exprString = satJson["algorithms"][alg]["expressionStringOverride"]

    # Wellenlaengen ersetzen
    i = 1
    for bandname in satJson["algorithms"][alg]["bands"]:
        bandwl = satJson["bandWl"][bandname]
        wlStr = str(bandwl)
        replacementStr = "l" + str(i)
        print("replacing " + replacementStr + " with " + wlStr)
        exprString = exprString.replace(replacementStr, wlStr)
        i += 1

    print("expressionString: " + exprString)
    return exprString


# Erzeugt das Dictionary, das notwendig ist für die Google Earth engine expressions
# ( https://developers.google.com/earth-engine/image_math#expressions )
# die keys sind die variablen namen, die in den expression strings verwendet werden, die values sind
# image bands, auf die die Variablennamen gemappt werden sollen. Ich glaub der Typ von den bands sind ee.Image
# Eingabe ist
# 	ein String (welcher Satellit)
# 	ein String (welcher Algorithmus)
# 	ein ee.Image?????? nein ich glaube es ist zu der Zeit noch eine ee.ImageCollection, super Benamsung...
# Returnwert ist ein dictionary
def getExpressionDict(sat, alg, satJson, image):
    dict = {}
    i = 1
    for bandname in satJson["algorithms"][alg]["bands"]:
        keyname = "L%d" % i
        dict[keyname] = image.select(bandname)
        i += 1
    print(dict)
    return dict


# https://developers.google.com/earth-engine/api_docs
# image kriegen für ein bestimmtes Datum, einen Satelliten, einen Algorithmus
# Eingabe ist
# 	ein String vom gewünschten Datum im Format yy-MM-dd
# 	ein String vom gewünschten Satelliten
# 	ein String vom gewünschten Algorithmus
# 	floating point werte vom gewünschten Minimum und Maximum Werte
# Returnwert ist (KEIN ee.Image, trotz des irreführenden Functionsnamens) eine google maps MapId, die von irgendeinem MapViewer am Client
# dazu verwendet werden kann, eine Map anzuzeigen
def getImage(d, sat, alg, mini, maxi, satJson, algStr):
    # date = ee.Date(d)
    desiredDate = ee.Date.parse("yyyy-MM-dd", d)
    halfDiff = (satJson["repeatInterval"] * 24) / 2;

    # eine Daterange erzeugen vom gewünschten Datum, die halbe Umlaufzeit vom Satelliten zurück und nach vor,
    # da sollt ma sicher zumindest ein Bild kriegen <- des is alles noch a weng a Schmarrn mit dem Datum
    dateRange = ee.DateRange(desiredDate.advance(-halfDiff, 'hour'), desiredDate.advance(halfDiff, 'hour'));

    # eine ImageCollection erzeugen, von dem richtigen Produkt für den Satelliten
    imageColl = ee.ImageCollection(satJson["bandDataImageString"])
    # nach unserem Datum filtern
    imageColl = imageColl.filterDate(dateRange)

    imageColl = imageColl.sort('system:time_start', False)

    # die einzelnen Streifen, die sich einer ImageCollection befinden, zamstückeln
    # ACHTUNG, danach ist es ein ee.Image und keine ImageCollection mehr!!! Ich hasse meine faule Variablenbenamsung
    imageColl = imageColl.mosaic()

    # welche Bänder sind verfügbar in diesem Image?
    # availbands ist eine liste
    availbands = imageColl.bandNames();

    if alg == 'RGB':

        # RGB Bänder holen
        imageColl = imageColl.select(satJson["algorithms"][alg]["bands"])
        # imageColl = imageColl.select(algorithmBands[sat][alg])
        # mapid holen
        mapId = imageColl.getMapId({'min': mini, 'max': maxi})
        acquisitionDate = imageColl.date();
        return (mapId)
    else:
        if sat == "Modis":
            # ein generisches Image erstellen, wo überall 1 drinsteht
            ones = ee.Image.constant(1.0)
            # die Bänder holen, die ich für den aktuellen Algorithmus brauchen
            # Ergebnis ist ein EE.IMAGE!!! zefix nochamal
            # das ist eigtl. der Unterschied zw. Modis und den anderen
            imageColl = imageColl.select(satJson["algorithms"][alg]["bands"])

        # den Expression string holen für den sat und den algo
        expr = getExpressionString(sat, alg, satJson, algStr)
        # expression dict holen (keys sind Variablennamen im expresionstring, values sind ee.Image mit einem Band)
        dict = getExpressionDict(sat, alg, satJson, imageColl)
        # expression für den Algorithmus auführen
        # Ergebnis ist wieder ein ee.Image
        algImg = imageColl.expression(expr, dict)

        # ein generisches Image erstellen, wo überall 1 drinsteht
        ones = ee.Image.constant(1.0)
        print("maxi/mini/maxi-mini" + "{:.2f}".format(maxi) + "/" + "{:.2f}".format(mini) + "/" + "{:.2f}".format(
            maxi - mini))
        # normalisieren auf min/max. Den Miniumwert abziehen, durch (maxi-mini) dividieren (jetzt hama mini-maxi auf 0-1 gemappt)
        normalizedAlg = algImg.subtract(mini).divide(maxi - mini)

        paletteList = ['000000', '0000FF', '00FFFF', '00FF00', 'FFFF00', 'FF0000']
        colorfulImg = normalizedAlg.visualize(min=0, max=1, palette=paletteList)

        if sat != "Modis":
            # die Wolkenmaske und Wassermaske holen (ee.Image)
            cloudMask = getCloudMask(dateRange, sat, satJson)
            waterMask = getWaterMask(dateRange, sat)
            # colorfulImg = colorfulImg.updateMask(cloudMask)

            # ein graues Bild erzeugen (generisch, also egal welche Grösse)
            grayImage = ee.Image.constant([128, 128, 128]);
            # überall wo wolken sind, grau malen
            colorfulImg = grayImage.where(cloudMask, colorfulImg)
            # überall wo kein wasser ist, maskieren (also transparent machen)
            colorfulImg = colorfulImg.updateMask(waterMask)

        # acquisitionDate = colorfulImg.date();

        urlfetch.set_default_fetch_deadline(30)
        socket.setdefaulttimeout(30)
        # mapid holen (das stösst die Berechnungen am Server eigtl. erst an), die der client braucht
        # min und max stellma auf 0 und 255, weil wir ein RGB Bild haben, dessen Werte zwischen 0.0 und 255.0 sind
        mapId = colorfulImg.getMapId({'min': 0, 'max': 255})

        return (mapId)
