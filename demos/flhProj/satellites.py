#!/usr/bin/env python
# coding=utf-8
"""A simple config file containing the values for the satellites. This is just one step towards proper modularization."""

# wie oft kommt der Satellit vorbei (bzw. eigtl. wie oft wird das Produkt auf GEE upgedated)
satelliteRepeatInterval =  {
  "Landsat7" : 16,
  "Landsat8" : 16,
  "Sentinel2" : 10,
  "Sentinel3": 2,
    "Modis" : 1
};

# von welchem GEE Produkt krieg ich die Cloud mask her
cloudMaskImageStrings = {
  "Landsat7" : 'LANDSAT/LE07/C01/T1',#'LANDSAT/LE7_L1T_TOA_FMASK',
  "Landsat8" : 'LANDSAT/LC08/C01/T1',#'LANDSAT/LC8_L1T_TOA_FMASK',
  "Sentinel2" : 'COPERNICUS/S2',
  "Sentinel3": 'COPERNICUS/S3/OLCI',
  "Modis": 'MODIS/006/MYD11A1'
};

# von welchem GEE Produkt krieg ich die snow mask her
snowMaskImageStrings = {
  "Landsat7" : 'LEDAPS/LE7_L1T_SR',
  "Landsat8" : 'LANDSAT/LC8_L1T_8DAY_NDSI',
  "Sentinel2" : 'LEDAPS/LE7_L1T_SR',
  "Sentinel3":  'LEDAPS/LE7_L1T_SR',
  "Modis": 'MODIS/006/MYD11A1'
};

# von welchem GEE Produkt krieg ich die eigtl. Banddaten her?
bandDataImageStrings = {
  "Landsat7" :  'LANDSAT/LE07/C01/T1',#'LANDSAT/LE7_L1T',
  "Landsat8" :  'LANDSAT/LC08/C01/T1',#'LANDSAT/LC8_L1T',
  "Sentinel2" : 'COPERNICUS/S2',
  "Sentinel3" :  'COPERNICUS/S3/OLCI',
  "Modis": 'MODIS/006/MYD11A1'
}

# welche Bänder brauch ich bei welchem Satelliten für welchen Algorithmus
algorithmBands = {
    "Landsat7" : {
        "FLH" : ['B2', 'B3', 'B4'],
        "BB34" : ['B3', 'B4'],
        "BB31" : ['B3', 'B1'],
        "BB21" : ['B2', 'B1'],
        "TSS_sb": ['B4'],
        "TSS_low_mb": ['B2', 'B3'],
        'TSS_high_mb': ['B2', 'B4']
    },
    "Landsat8" : {
        "BB31" : ['B4', 'B2'],
        "RGB" : ['B4', 'B3', 'B2'],
        "TSS_sb": ['B5'],
        "TSS_low_mb": ['B3', 'B4'],
        'TSS_high_mb': ['B3', 'B5'],
        'SWM': ['B1', 'B3']
    },
    "Sentinel2" : {
        "FLH" : ['B4', 'B5', 'B6'],
        "RLH" : ['B4', 'B5', 'B7'],
        "CB1" : ['B5', 'B6'],
        "TBA" : ['B4', 'B5', 'B6'],
        "MCI" : ['B4', 'B5', 'B6'],
        "RGB" : ['B4', 'B3', 'B2'],
        "TSS_sb": ['B5'],
        "TSS_low_mb": ['B2', 'B3'],
        'TSS_high_mb': ['B3', 'B8'],
        'SWM': ['B1', 'B3']
    },
    "Sentinel3" : {
        "FLH" : ['Oa08_radiance', 'Oa10_radiance', 'Oa11_radiance'], #'b8', 'b10', 'b11'
        "FLH2" : ['Oa08_radiance', 'Oa09_radiance', 'Oa10_radiance'], #'b8', 'b10', 'b11'
        "RLH" : ['Oa10_radiance', 'Oa11_radiance', 'Oa18_radiance'],#'b10', 'b11', 'b18'
        "MCI" : ['Oa10_radiance', 'Oa11_radiance', 'Oa12_radiance'],#'b10', 'b11', 'b12'
        "TSS_sb": ['Oa07_radiance'],
        "TSS_low_mb": ['Oa05_radiance', 'Oa06_radiance'],
        'TSS_high_mb': ['Oa06_radiance', 'Oa17_radiance'],
        'RGB': ['Oa08_radiance', 'Oa06_radiance', 'Oa04_radiance']
    },
    "Modis": {
        "Brightness_temperature_day": ['LST_Day_1km'],
        "Brightness_temperature_night": ['LST_Night_1km'],
    }
}

# diesselbe Tabelle wie algorithmBands, nur stehen jetzt die Wellenlängen der Bänder drinnen, statt 
# die Band namen
# TODO: lieber einfach eine Tabelle mit allen Satelliten und
# allen Baendern machen, als da rumzuscheissen.
algorithmWl = {
    "Landsat7": {
        "FLH": [565, 660, 825],
        "BB34": [660, 825],
        "BB31": [660, 482.5],
        "BB21": [565, 482.5],
    },
    "Landsat8": {
        "BB31": [654.59, 482.04],
        "RGB": [654.59, 561.41, 482.04]
    },
    "Sentinel2": {
        "FLH": [665.0, 705.0, 740.0],
        "RLH": [665, 705, 783],
        "CB1": [705, 740],
        "TBA": [665, 705, 740],
        "MCI": [665.0, 705.0, 740.0],
        "RGB": [654.59, 561.41, 482.04]
    },
    "Sentinel3": {
        "FLH": [665.0, 681.25, 708.0],
        "FLH2": [665.0, 673.75, 681.25],
        "RLH": [681.25, 708.75, 885],
        "MCI": [681.25, 708.75, 753.0],
        "TSS_sb": [620.0],
        "TSS_low_mb": [510.0, 560.0],
        'TSS_high_mb': [560.0, 865.0],
        'RGB' : [665.0, 560.0, 490.0]
    },
    "Modis": {
        "Brightness_temperature_day": [666.0], #dummy werte, wird nicht verwendet, ist ein Produkt aus mehr Bändern
        "Brightness_temperature_night": [777.0]
    }
}

# Welche Default Min/Max Werte sollen eingestellt sein für welche Algorithmus bei welchem Satelliten
algorithmMinMax = {
    "Landsat7": {
        "FLH": [-16,64],
        "BB34": [0, 3.0],
        "BB31": [0.2, 1.0],
        "BB21": [0.5, 1],
        "TSS_sb": [-100.0, 100.0],
        "TSS_low_mb": [-1.0, 1.0],
        "TSS_high_mb": [-1.0, 1.0],
    },
    "Landsat8": {
        "BB31": [0.5,0.9],
        "RGB": [0.0, 16000.0],
        "TSS_sb": [4000, 8000],
        "TSS_low_mb": [-1.0, 1.0],
        "TSS_high_mb": [-1.0, 1.0],
        "SWM": [-100.0,100.0],
    },
    "Sentinel2": {
        "FLH": [-100.0,100.0],
        "RLH": [-100.0,100.0],
        "CB1": [0.5, 2.2],
        "TBA": [-0.3,0.0],
        "MCI": [-100.0,100.0],
        "RGB": [0.0, 4096.0],
        "TSS_sb": [0.0, 1024.0],
        "TSS_low_mb": [-1.0, 1.0],
        "TSS_high_mb": [-1.0, 1.0],
        "SWM": [-100.0,100.0],
    },
    "Sentinel3": {
        "FLH": [-10, 10.0],
        "RLH": [-20.0, 20.0],
        "MCI": [-5.0, 5.0],
        "TSS_sb": [0.0, 4096.0],
        "TSS_low_mb": [-1.0, 1.0],
        "TSS_high_mb": [-1.0, 1.0],
        "RGB" : [0.0, 256.0]
    },
    "Modis": {
        "Brightness_temperature_day": [0, 40.0],
        "Brightness_temperature_night": [0.0, 40.0]
    },
    "Modis_Bands" : {
        "RGB": [0.0, 255.0],
        "BG21": [0.0, 40.0],
        "TSS_sb": [0.0, 40.0]
    }
}

