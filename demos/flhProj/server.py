#!/usr/bin/env python
# coding=utf-8
"""A simple example of connecting to Earth Engine using App Engine."""



# Works in the local development environment and when deployed.
# If successful, shows a single web page with the SRTM DEM
# displayed in a Google Map.  See accompanying README file for
# instructions on how to set up authentication.

import os

import config
import ee
import jinja2
import webapp2
import json
import datetime
from google.appengine.api import urlfetch
import socket

#from satellites import *  # satellite config

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

from glwfunctions import * # helper function definition
from statistics import *
from geotiffdownload import *

class MainPage(webapp2.RequestHandler):

  def get(self):                             # pylint: disable=g-bad-name

    urlfetch.set_default_fetch_deadline(30)
    socket.setdefaulttimeout(30)
    """Request an image from Earth Engine and render it to a web page."""
    ee.Initialize(config.EE_CREDENTIALS)
    mapid = None
    #mapid = ee.Image('srtm90_v4').getMapId({'min': 0, 'max': 1000})

    print("before request")
    print(self.request)
    currentSatellite = "Landsat8"
    ten_days = datetime.timedelta(days=10)
    currentDateStr = (datetime.datetime.today() - ten_days).strftime('%Y-%m-%d')
    acquisitionDateStr = "2017-03-27"
    currentAlgorithm = "RGB"

    isReload = False

    isFastViewer = False
    makeDownload = False
    downloadColorImage = False
    makeStatistics = False

    if self.request.get('date') != '':
        currentDateStr=self.request.get('date')
        print("date is" + currentDateStr)
    if self.request.get('Satellite') != '':
        currentSatellite = self.request.get('Satellite')
        print("Satellite is" + currentSatellite)
    if self.request.get('Algorithm') != '':
        currentAlgorithm = self.request.get('Algorithm')
        print("Algorithm is" + currentAlgorithm)

    jsondata  = json.load(open('satellites.json'))
    currentSatJson = jsondata["satellites"][currentSatellite]
    currentAlgJson = jsondata["algorithmExpressionStrings"][currentAlgorithm]

    currentMin = currentSatJson["algorithms"][currentAlgorithm]["minMax"][0]
    currentMax = currentSatJson["algorithms"][currentAlgorithm]["minMax"][1]

    print("currenMin: " + str(currentMin))

    if self.request.get('min') != '':
        currentMin = self.request.get('min')
        print("min is" + currentMin)
    if self.request.get('max') != '':
        currentMax = self.request.get('max')
        print("max is" + currentMax)

    #jsonfile = open('satellites.json', 'r')
    #print(jsonfile);
    
    print("Satellite for getImage" + str(currentSatellite))
    mapid = getImage(currentDateStr, currentSatellite, currentAlgorithm, float(currentMin), float(currentMax), currentSatJson, currentAlgJson)

    # acquisitionDateStr = acquisitionDate.getInfo().serialize()
    # print(acquisitionDateStr)

    currentLat = 46.867
    currentLong = 17.693
    currentZoom = 8
    currentOpacity = 80
    currentLanguage = "English"
    justGetDate = False
    if self.request.get('justGetDate') == 'true':
        justGetDate = True
    if self.request.get('language') != '':
        currentLanguage = self.request.get('language')
        print("language is" + str(currentLanguage))
    if self.request.get('lat') != '':
    	print("latitude in get " + self.request.get('lat'))
    	currentLat = self.request.get('lat')
        print("latitude is" + str(currentLat))
    if self.request.get('long') != '':
        currentLong = self.request.get('long')
        print("longi is" + str(currentLong))
    if self.request.get('zoom') != '':
        currentZoom = self.request.get('zoom')
        print("zoom is" + str(currentZoom))
    if self.request.get('opacity') != '':
        currentOpacity = self.request.get('opacity')
        print("opac is" + str(currentOpacity))
    if self.request.get('reload') == 'true':
        isReload=True
    if self.request.get('fastViewer') == 'true':
        isFastViewer = True
    if self.request.get('download') == 'true':
        makeDownload = True
    if self.request.get('colorful') == 'true':
        downloadColorImage = True
    if self.request.get('statistics') == 'true':
        makeStatistics = True
        
    nelat = 46.867
    nelng = 17.693
    swlat = 46.867
    swlng = 17.693
    if self.request.get('nelat') != '':
        nelat = self.request.get('nelat')
    if self.request.get('nelng') != '':
        nelng = self.request.get('nelng')
    if self.request.get('swlat') != '':
        swlat = self.request.get('swlat')
    if self.request.get('swlng') != '':
        swlng = self.request.get('swlng')


    # These could be put directly into template.render, but it
    # helps make the script more readable to pull them out here, especially
    # if this is expanded to include more variables.
    template_values = {
        'mapid': mapid['mapid'],
        'token': mapid['token'],
        'algorithm' : currentAlgorithm,
        'satellite' : currentSatellite,
        'dateStr' : currentDateStr,
        'acDateStr' : acquisitionDateStr,
        'minStr': currentMin,
        'maxStr': currentMax,
        'latStr' : currentLat,
        'longStr' : currentLong,
        'zoomStr' : currentZoom,
        'opacityStr' : currentOpacity,
        'nelat' : nelat,
        'nelng' : nelng,
        'swlat' : swlat,
        'swlng' : swlng,
        'downloadUrl' : "nix.html",
        'statisticsStr' : "{}",
        'language': currentLanguage
    }

    #template = jinja_environment.get_template('eeclient.html')
    template = jinja_environment.get_template('index.html')
    if isFastViewer:
        print("in FASTVIEWER")
        template = jinja_environment.get_template('eeclient.html')
    if makeStatistics:
        isReload = False
    	print("in STATISTICS")
    	template_values['statisticsStr'] = calcStatisticsAroundPoint( template_values, currentSatJson, currentAlgJson, 90)
    	template = jinja_environment.get_template("StatisticsData.json")
    if justGetDate:
        isReload = False
        print("in JUSTGETDATE")
        template_values['statisticsStr'] = getDateAroundPoint( template_values, currentSatJson, currentAlgJson, currentSatJson["repeatInterval"])
    	template = jinja_environment.get_template("StatisticsData.json")
    if isReload:
        print("in RELOAD")
        #template_values['statisticsStr'] = calcStatisticsAroundPoint( template_values, currentSatJson, currentAlgJson)
        template = jinja_environment.get_template('RequestData.json')
    if makeDownload:
        print("in DOWNLOAD")
        print("satellite is " + currentSatellite + " alg is " + currentAlgorithm) 
        template_values['downloadUrl'] = getGeotiffDownloadinRegion(template_values, currentSatJson, currentAlgJson, downloadColorImage)
        template = jinja_environment.get_template("DownloadRequestData.json")
        
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')
    self.response.headers.add_header('Access-Control-Allow-Headers', 'Content-Type')
    #self.response.headers['Content-Type'] = 'image/png'
    #self.response.headers['Content-Type'] = 'image/jpeg'
    #self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(template.render(template_values))

app = webapp2.WSGIApplication([('/', MainPage)], debug=True)
