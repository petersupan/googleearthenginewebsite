
// welche Algos gibts für welchen Satelliten
var satSelects = {
    "Landsat7": [
        "FLH",
        "BB34",
        "BB31",
        "BB21",
        "TSS_sb",
        "TSS_low_mb",
        'TSS_high_mb'
    ],
    "Landsat8": [
        "BB31",
        "RGB",
        "TSS_sb",
        "TSS_low_mb",
        'TSS_high_mb',
        'SWM',
        "Brightness_temperature_day"
    ],
    "Sentinel2": [
        "FLH",
        "RLH",
        "CB1",
        "TBA",
        "MCI",
        "RGB",
        "TSS_sb",
        "TSS_low_mb",
        'TSS_high_mb',
        'SWM'
    ],
    "Sentinel3": [
        "FLH",
        "RLH",
        "MCI",
        "TSS_sb",
        "TSS_low_mb",
        "TSS_high_mb",
        "RGB"
    ],
    "Modis": [
        "Brightness_temperature_day",
        "Brightness_temperature_night"
    ],
    //"Modis_Bands": [
    //    "RGB",
    //    "BB21",
    //    "TSS_sb"
    //]
};

// welche algos gibts für welches Produkt
var productAlgorithms =  {
      	"Chlorophyll": [
      		"FLH",
      		"MCI",
      		"RLH",
        	"BB34",
       		"BB31",
        	"BB21",
      	],
  		"Sediment": [
  			"TSS_sb",
        	"TSS_low_mb",
        	'TSS_high_mb'
  		],
  		"Temperature": [
  			"Brightness_temperature_day",
  			"Brightness_temperature_night"
  		],
  		"visibleColor":[
  			"RGB"
  		],
  		"SWM": [
  			"SWM"
  		],
	};
	
// welche Satelliten haben welches Produkt
var productSatellites = {
	   "Chlorophyll": [
      		"Landsat7",
      		"Landsat8",
      		"Sentinel2",
        	"Sentinel3"
        	//"Modis_Bands"
      	],
  		"Sediment": [
      		"Landsat7",
      		"Landsat8",
      		"Sentinel2",
        	"Sentinel3"
        	//"Modis_Bands"
  		],
  		"Temperature": [
  			"Modis",
            "Landsat8"
  		],
  		"visibleColor":[
      		"Landsat8",
      		"Sentinel2",
        	"Sentinel3"
        	//"Modis_Bands"
  		],
  		"SWM": [
  			"Landsat8",
  			"Sentinel2"
  		],
	};
	