// neede because on google cloud shell I have to hardcode the url
var getBaseUrl = function() {
	var gcloudUrl = "https://8080-dot-3280503-dot-devshell.appspot.com";
	//var normalUrl = window.location.href;
    var normalUrl = window.location.hostname;
	return normalUrl;
}

var dateCalcInAction = false;
var dateCalcScheduled = false;

var g_map;

// Initialize the Google Map and add our custom layer overlay.
var initialize = function(mapId, token, latStr, longStr, zoomStr, opacityStr) {
  // The Google Maps API calls getTileUrl() when it tries to display a map
  // tile.  This is a good place to swap in the MapID and token we got from
  // the Python script. The other values describe other properties of the
  // custom map type.
  var eeMapOptions = {
    getTileUrl: function(tile, zoom) {
      var baseUrl = 'https://earthengine.googleapis.com/map';
      var url = [baseUrl, mapId, zoom, tile.x, tile.y].join('/');
      //console.log("tile: " +tile.x + "  /  " + tile.y+ " zoom: " + zoom + " mapid: " + mapId);
      url += '?token=' + token;
      //console.log("finalurl " + url);
      return url;
    },
    tileSize: new google.maps.Size(256, 256)
  };

  console.log("in init " + mapId + " " + token);


  // Create the map type.
  var mapType = new google.maps.ImageMapType(eeMapOptions);
  var zoom = parseInt (zoomStr, 10);

  var myLatLng = new google.maps.LatLng(parseFloat(latStr), parseFloat(longStr));//(46.867, 17.693);
  var mapOptions = {
    center: myLatLng,
    zoom: zoom,
    maxZoom: 14,
    streetViewControl: false
  };

  // Create the base Google Map.
  var map = new google.maps.Map(
      document.getElementById('map'), mapOptions);

  // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    if (input == null) {
        console.log("VERFLUCHTE SEARCHBOX!!!!!!!!!!!!");
        var container = document.getElementById('searchboxdiv');
        // Create an <input> element, set its type and name attributes
                var input = document.createElement("input");
                input.id = "pac-input";
                input.class = "controls";
                input.type="text";
                input.placeholder="Search Box";
                container.appendChild(input);
    } else {
        console.log("searchbox found!!!!!!!!!!!!!!!!!!");
    }

    if (input == null) {
        console.log("VERFLUCHTE SEARCHBOX22222222!!!!!!!!!!!!");
    }
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length === 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

  mapType.setOpacity(parseFloat(opacityStr)/100.0);

  // Add the EE layer to the map.
  map.overlayMapTypes.push(mapType);

  map.addListener('center_changed', function() {
    var latField = document.getElementById("SatForm").lat;
    var longField = document.getElementById("SatForm").long;

    latField.value = map.getCenter().lat();
    longField.value = map.center.lng();

    recalcCurrentDate();
  });

  map.addListener('zoom_changed', function() {
    var zoomField = document.getElementById("SatForm").zoom;
    zoomField.value = map.getZoom();
  });

  mapControl = new MapControl(map, mapType);

  changeAlgorithmDesc();
  changeSatelliteDesc();
  
  g_map = map;
};

/** @constructor **/
function MapControl(map, overlay1) {
    this.map = map;
    this.overlay1 = overlay1;
}

MapControl.prototype.opacityChanged = function(opacity) {
    //console.log("in opacityChanged, change op to " + opacity);
    this.overlay1.setOpacity(parseFloat(opacity)/100.0);
};

var encodeToUri = function(params) {
    var esc = encodeURIComponent;
    //var query = Object.keys(params)
    //.map(k => esc(k) + '=' + esc(params[k]))
    //.join('&');
    var url = "?";
    for (var key in params) {
        console.log(params[key]);
        url += key;
        url+= "=";
        url += esc(params[key]);
        url += "&";
    }
    url = url.substring(0, url.length - 1);
    return url;
};

var getIdxOfSelect = function(select, value) {
      var opts = select.options;
      var idx = 0;
      for(var i = 0; i < opts.length; i++) {
        var item = opts[i];
        if (item.value === value) {
          console.log("index of tba is " + i);
          idx = i;
          return idx;
        }
      }
      return idx;
};



var dateIntervals = {
    "Landsat7" : 8,
    "Landsat8" : 8,
    "Sentinel2" : 4,
    "Sentinel3" : 2,
    "Modis": 1,
    "Modis_Bands": 1
};

console.log("currentLang is " + currentLang);
var satelliteInfos = languages[currentLang]["satelliteInfos"];
var algorithmInfos = languages[currentLang]["algorithmInfos"];

var setOptionsForState = function(select, dict) 
{
    var opts = select.options;
    var backup= -1;
    for(var i = 0; i < opts.length; i++) {
        var item = opts[i];
        opts[i].hidden = true;
        for(var a = 0; a < dict.length; a++) {
            if (item.value === dict[a]) {
                opts[i].hidden = false;
                if (backup === -1) {
                    backup = i;
                }
            }
        }
    }
    // set first valid algorithm
    if (opts[opts.selectedIndex].hidden === true) {
        select.selectedIndex = backup;
    }
}

var setSatellitesForProduct = function() {
	var prodSelect = document.getElementById("SatForm").Product;
    var selectedProdIdx = prodSelect.options.selectedIndex;
    var selectProdName = prodSelect.options[selectedProdIdx].value;

    var satSel = document.getElementById("SatForm").Satellite;
    //var selectedSatIdx = satSel.options.selectedIndex;
    //var selectSatName = prodSelect.options[selectedSatIdx].value;
    
    var dict = productSatellites[selectProdName];
    setOptionsForState(satSel, dict);
}

// algorithmen, dies für das Produkt nicht gibt, aus der Liste löschen
var setAlgorithmsForProductAndSat = function(select, product, satellite) {
	var algsForProdList = productAlgorithms[product];
	var algsForSatList = satSelects[satellite];
	console.log("satellitename ");
	console.log("satellitename " + satellite);
	console.log("algsforsalist " + algsForSatList);
    var opts = select.options;
    var backup= -1;
    // für alle einträge im select
    for(var i = 0; i < opts.length; i++) {
        var item = opts[i];
        opts[i].hidden = true;
        var inprod = false;
        var insat = false
        // tauchts in der liste fürs product auf?
        for(var a = 0; a < algsForProdList.length; a++) {
            if (item.value === algsForProdList[a]) {
                //console.log("enabled item" + item.value);
                inprod = true;
            }
        }
        
        // tauchts in der liste fürn satellite auf?
        for(var a = 0; a < algsForSatList.length; a++) {
            if (item.value === algsForSatList[a]) {
				insat = true;
            }
        }
        
        // nur sichtbar, wenns sowohl für das Produkt ist, als auch von dem Sat unterstützt wird
        if (inprod && insat) {
        	opts[i].hidden = false;
        	if (backup === -1) {
        		backup = i;
        	}
        }
    }
    // set first valid algorithm
    if (opts[opts.selectedIndex].hidden === true) {
        select.selectedIndex = backup;
    }
}

var setAlgorithmsForCurrentProductAndSat = function() {
	var prodSelect = document.getElementById("SatForm").Product;
    var selectedProdIdx = prodSelect.options.selectedIndex;
    var selectProdName = prodSelect.options[selectedProdIdx].value;

    var satSel = document.getElementById("SatForm").Satellite;
    var selectedSatIdx = satSel.options.selectedIndex;
    var selectSatName = satSel.options[selectedSatIdx].value;
    
    var algSel = document.getElementById("SatForm").Algorithm;
    
    console.log("selectsatidcx name " + selectedSatIdx + "   " + selectSatName);
    setAlgorithmsForProductAndSat(algSel, selectProdName, selectSatName);
}

function formSubmit(resetMinMax, recalcStatistics, fullReload, justGetDate) {
    console.log("formsubmit ");
    
    var urlDic = {};

    var lang = document.getElementById("langSel").value;
    //var fullReload = false;
    if (fullReload) {
        console.log("lang ist " + lang + " currentLang ist " + currentLang);
        currentLang = lang;
        urlDic["reload"] = "false";
        fullReload= true;
    }else {
        urlDic["reload"] = "true";
    }
	
	urlDic["statistics"] = recalcStatistics;	
	
	var prodSelect = document.getElementById("SatForm").Product;
	var selectedProductId = prodSelect.selectedIndex;
	var selectProdName = prodSelect.options[selectedProductId].value;
	
	setSatellitesForProduct();

	var satSelect = document.getElementById("SatForm").Satellite;
    var selectedSatIdx = satSelect.options.selectedIndex;
    var selectSatName = satSelect.options[selectedSatIdx].value;
    urlDic["Satellite"] = satSelect.options[selectedSatIdx].value;

    var algSelect = document.getElementById("SatForm").Algorithm;
    
    setAlgorithmsForCurrentProductAndSat();

    var selectedAlgIndex = algSelect.selectedIndex;
    urlDic["Algorithm"] = algSelect.options[selectedAlgIndex].value;

    var dateInp = document.getElementById("SatForm").date;
    urlDic["date"] = dateInp.value;

    if(resetMinMax) {
    //console.log("resetting min and max");
        urlDic["min"] = "";
        urlDic["max"] = "";
    } else {
    //console.log("no reset min and max");
        var minInp = document.getElementById("SatForm").min;
        urlDic["min"] = minInp.value;

        var maxInp = document.getElementById("SatForm").max;
        urlDic["max"] = maxInp.value;
    }

    var latInp = document.getElementById("SatForm").lat;
    urlDic["lat"] = g_map.getCenter().lat();//latInp.value;
    console.log("LAT: " + latInp.value);

    var lngInp = document.getElementById("SatForm").long;
    urlDic["long"] = g_map.getCenter().lng();//lngInp.value;//

    var opcInp = document.getElementById("SatForm").opacity;
    //console.log("opacity was: " + opcInp.value);
    urlDic["opacity"] = opcInp.value;
    //console.log("opacity is now: " + parseFloat(opcInp.value));

    var zoomInp = document.getElementById("SatForm").zoom;
    urlDic["zoom"] = zoomInp.value;

    urlDic["language"] = currentLang;

    if (justGetDate) {
        urlDic["justGetDate"] = "true";
    }

    var url = encodeToUri(urlDic);
    console.log(url);

    var request = "https://" + getBaseUrl() + url;
    if(fullReload == false) {
        if (justGetDate) {
            console.log("in JUSTGETDATE")
            
            httpGetAsync(request, dateResponseCallback);
        } else {
            httpGetAsync(request, responseCallback);
            recalcCurrentDate();
        }
        
    } else {
        console.log("window.location.hostname: " + window.location.hostname);
        window.location.replace("https://" + getBaseUrl() + url);
    }
}

function httpGetAsync(theUrl, callback)
{
    console.log("httpgetAsync " + theUrl);
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200)
            callback(xmlHttp.responseText);
        else {
            console.log("onreadzstatechanged " + xmlHttp.readyState + " " + xmlHttp.status);
        }
    };
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function guiInitialize(templ) {
    var form = document.getElementById("SatForm");

    var satSel = form.Satellite;
    var satSelIdx = getIdxOfSelect(satSel, templ["satellite"]);
    satSel.selectedIndex = satSelIdx;

    var algSel = form.Algorithm;
    var algSelIdx = getIdxOfSelect(algSel, templ["algorithm"]);
    algSel.selectedIndex = algSelIdx;

    var latInp = form.lat;
    latInp.value = templ["latStr"];

    var longInp = form.long;
    longInp.value = templ["longStr"];

    var minInp = form.min;
    minInp.value = templ["minStr"];

    var maxInp = form.max;
    maxInp.value = templ["maxStr"];

    var dateInp = form.date;
    dateInp.value = templ["dateStr"];

    var zoomInp = form.zoom;
    zoomInp.value = templ["zoomStr"];

    var opacityInput = form.opacity;
    opacityInput.value = templ["opacityStr"];
}

function updateViz(templ) {
	var TESTER = document.getElementById('plot');
    Plotly.deleteTraces(TESTER, 0);
    
    var jsonList = JSON.parse(templ["statisticsStr"]);
    var dateList = [];
    var valList = [];
    var xList = [];

    var tickvalList = [];
    var ticktextList= [];
	for(var i = 0; i < jsonList.length; i++) {
        valList.push(jsonList[i][0])
        var date = new Date(jsonList[i][1])
		dateList.push(date);
        xList.push(i);
	}

    for(var i = 0; i < jsonList.length; i+= 3) {
        var date = new Date(jsonList[i][1])
        tickvalList.push(i);
        ticktextList.push(date);
	}

    var data =  [{
            x: xList,
            y: valList,
            text: dateList,
            mode: 'lines+markers',
            connectgaps: true
        }];

        	var prodSelect = document.getElementById("SatForm").Product;
    var selectedProdIdx = prodSelect.options.selectedIndex;
    var selectProdName = prodSelect.options[selectedProdIdx].value;
   
   var layout = {
            //   title: {
            //     text:"Plot Title",
            //     font: {
            //         family: 'Courier New, monospace',
            //         size: 24,
            //         color: "black"
            //     },

            // },
            title: "Product: " + selectProdName +" Satellite: " + templ["satellite"] + " Algorithm: " + templ["algorithm"],
            xaxis: {tickvals:tickvalList, ticktext :ticktextList},
            margin: { t: 50 } 
        };
	Plotly.newPlot( TESTER, data, 
         layout
        );

Plotly.newPlot(TESTER, data, layout);

}

function downloadURI(uri, name) 
{
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}

// function downloadURI(dataurl, filename) {
//   var a = document.createElement("a");
//   a.href = dataurl;
//   a.setAttribute("download", filename);
//   var b = document.createEvent("MouseEvents");
//   b.initEvent("click", false, true);
//   a.dispatchEvent(b);
//   return false;
// }

function dateResponseCallback(responseText) {
    console.log("response text of date get request is " + responseText);
    var obj = JSON.parse(responseText);
    var statisticsList = JSON.parse(obj["templValues"]["statisticsStr"]);
    var dateText = statisticsList[statisticsList.length-1][1];
    console.log("StatisticsList: " + statisticsList[0]);
    var date = new Date(dateText)
    var currentDateShowLabel = document.getElementById("currentDateShowLabel");
    currentDateShowLabel.innerText = date.toISOString();
    dateCalcInAction = false;

}

function responseCallback(responseText) {
    console.log("response text of get request is " + responseText);
    var obj = JSON.parse(responseText);
    if(obj["templValues"].hasOwnProperty('downloadUrl')){
    	console.warn("downloadurl in responsecallback")
	} else {		
	    //console.log("json obj is " + JSON.stringify(obj));
	
	    var test = obj["templValues"]["algorithm"];
	
	    var templ = obj["templValues"];
	
	    initialize(templ["mapid"], templ["token"], templ["latStr"], templ["longStr"], templ["zoomStr"], templ["opacityStr"]);
	    updateViz(templ);
	
	    guiInitialize(templ);
    }

}

function formKeyPress(e) {
    if(e.which === 13) {
        console.log("enter key pressed, submit");
        formSubmit(false, false, false, false);
        return false;
    } else {
        return true;
    }
}

function changeSatelliteDesc() {
    var satSelect = document.getElementById("SatForm").Satellite;

    var i = satSelect.selectedIndex;
    var selectedSat = satSelect.options[i].value;

    document.getElementById("satinfo").innerHTML = satelliteInfos[selectedSat];
    console.log("changed sat desc to " + satelliteInfos[selectedSat]);
}

function changeAlgorithmDesc() {
    var algSelect = document.getElementById("SatForm").Algorithm;

    var i = algSelect.selectedIndex;
    var selectedAlg = algSelect.options[i].value;

    document.getElementById("alginfo").innerHTML = algorithmInfos[selectedAlg];
    console.log("changed alg desc to " + algorithmInfos[selectedAlg]);
}

function advanceDate(direction) {
    var satSelect = document.getElementById("SatForm").Satellite;
    var i = satSelect.selectedIndex;
    var satName = satSelect.options[i].value;
    var dateInterval = dateIntervals[satName];

    var dateInp = document.getElementById("SatForm").date;
    var dateStr = dateInp.value;

    var curDate = new Date(dateStr);
    console.log("current date is" + curDate);

    var advDate= curDate;

    if(direction === "forward") {
        advDate.setDate(advDate.getDate()+dateInterval);
    } else {
        advDate.setDate(advDate.getDate()-dateInterval);
    }
    console.log("advanced date " +direction+ " is" + advDate);

    var isoStr = advDate.toISOString();
    var dateIso = isoStr.split("T")[0];
    dateInp.value=dateIso;
    console.log(dateIso);

    formSubmit(false, false, false, false);
}

function downloadGeotiff(colorful = false) {
	var lat0 = g_map.getBounds().getNorthEast().lat();
	var lng0 = g_map.getBounds().getNorthEast().lng();
	var lat1 = g_map.getBounds().getSouthWest().lat();
	var lng1 = g_map.getBounds().getSouthWest().lng();
    
    var satSelect = document.getElementById("SatForm").Satellite;
    var selectedSatIdx = satSelect.options.selectedIndex;
    var satl = satSelect.options[selectedSatIdx].value;

    var algSelect = document.getElementById("SatForm").Algorithm;
    var selectedAlgIndex = algSelect.selectedIndex;
    var alg = algSelect.options[selectedAlgIndex].value;

    var dateInp = document.getElementById("SatForm").date;
    var date = dateInp.value;

    var zoomInp = document.getElementById("SatForm").zoom;
    var zoom = zoomInp.value; 
	
    urlDic = {
        "download" : "true",
        "nelat" : String(lat0),
        "nelng" :String(lng0),
        "swlat" :String(lat1),
        "swlng" :String(lng1),
        "Satellite" : String(satl),
        "Algorithm" : String(alg),
        "date" : String(date),
        "zoom" : String(zoom),
        "colorful": "false"
    };
    if (colorful) {
        urlDic["colorful"] = "true";
    }
    var url = encodeToUri(urlDic);
    
    console.log("Url for download Geotiff is \n" + url);
    
    //var requ = getBaseUrl() + url;
    var requ = "https://" + getBaseUrl() + url;
    httpGetAsync(requ, downloadCallback);

}


function downloadCallback(responseText) {
    console.log("download response text " + responseText);
    var obj = JSON.parse(responseText);

	var downloadUrl = obj["templValues"]["downloadUrl"];
	downloadURI(downloadUrl, "testdownload.zip");
}

function changeLanguage() {
    currentLang = document.getElementById("langSel").value;

    formSubmit(false, false, true, false);
}

function recalcCurrentDate(scheduled = false) {
    if (!dateCalcInAction) {
        console.log("starting date calc");
        formSubmit(false, false, false, true);
        dateCalcInAction = true;
        if(scheduled) 
            dateCalcScheduled = false;
    } else {
        if (!dateCalcScheduled) {
            console.log("datecalc still in action, running again in 5sec");
            setTimeout(() => recalcCurrentDate(true), 5000);
            dateCalcScheduled = true;
        } else {
            if(scheduled) {
                console.log("datecalc took longer than 5sec, try again in 5sec");
                setTimeout(() => recalcCurrentDate(true), 5000);
            }
        }
    }
}


function saveMapToDataUrl() {
    var node = document.getElementById('map');

    domtoimage.toPng(node)
        .then(function (dataUrl) {
            console.log("domToImageFinished");
            var img = new Image();
            img.src = dataUrl;
            document.body.appendChild(img);
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}