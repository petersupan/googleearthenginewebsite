#!/usr/bin/env python
# coding=utf-8
"""Functions for statistics."""

import os

import config
import ee
import jinja2
import webapp2
import datetime
from google.appengine.api import urlfetch
import socket
import json
import math
from satellites import *
from glwfunctions import *

_sat = ""
_alg = ""
_satJson = ""
_algStr = ""
_region = ""


def applyAlgorithm(img):
    img = ee.Image(img)
    # den Expression string holen für den sat und den algo
    expr = getExpressionString(_sat, _alg, _satJson, _algStr)
    # expression dict holen (keys sind Variablennamen im expresionstring, values sind ee.Image mit einem Band)
    dict = getExpressionDict(_sat, _alg, _satJson, img)
    # expression für den Algorithmus auführen
    # Ergebnis ist wieder ein ee.Image
    algImg = img.expression(expr, dict)
    return algImg


def dateGetter(img):
    img = ee.Image(img)
    date = img.date()
    return date


def meanReduceRegion(img):
    img = ee.Image(img)
    res = img.reduceRegion(ee.Reducer.mean(), _region);
    return res;


#@brief returns a list with each two elements, the mean value in this region and the timestamp.
# the timestamp is milliseconds from epoch, can be concerted to something sensible by the datetime object
# in python or the Date object in javascript
def getStatistics(d, sat, alg, satJson, algStr, region, nrDays):
    desiredDate = ee.Date.parse("yyyy-MM-dd", d)
    # eine Daterange erzeugen vom gewünschten Datum - 90 Tage, dann hamma 3 Monate
    dateRange = ee.DateRange(desiredDate.advance(-nrDays, 'day'), desiredDate)

    # richtige imagecollection erzeugen, gleich filtern
    imageColl = ee.ImageCollection(bandDataImageStrings[sat]).filterDate(dateRange).filterBounds(region)
    imageColl = imageColl.sort('system:time_start', False)

    # globale variablen setzen, die brauchma für die map functions
    global _sat
    _sat = sat
    global _alg
    _alg = alg
    global _satJson
    _satJson = satJson
    global _algStr
    _algStr = algStr
    global _region
    _region = region

    # get timestampps this is an opaque ee computedobject, not able to get anything out of it
    computedObject = imageColl.aggregate_array("system:time_start")

    #if we call getInfo, we get the server side object as python list, takes a while
    coInfo = computedObject.getInfo()

    afterAlgorithm = imageColl.map(applyAlgorithm)
    list = afterAlgorithm.toList(100)
    meanList = list.map(meanReduceRegion)

    res = []
    info = meanList.getInfo()
    
    myKey = ""

    # namen vom Key holen
    for key, value in info[0].iteritems():
        myKey = key
    print(myKey)

    for x in info[:]:
        res.append(x[myKey])

    print(res)
    print(info)

    print("lengths: ")
    print(len(res))
    print(len(coInfo))

    result = []
    for x in range(0, len(res)):
        date = datetime.datetime.fromtimestamp(int(coInfo[x] / 1000))
        print(date)
        result.append([res[x], coInfo[x]])

    return result


# @brief calculate on which y pixel (starting from north pole) the current latitude lands
# in zoom level 0 the entire earth (nortphole to southpole) is visible in 256 pixels (one tile)
def lat2yPixel(lat, zoom):
    # with every zoom level the number of pixels  doubles
    n = math.pow(2.0, zoom);
    # math function work with radians, not degrees
    lat_rad = math.radians(lat);
    yPix = n * (1.0 - (math.log(math.tan(lat_rad) + (1.0 / math.cos(lat_rad))) / math.pi)) / 2.0;
    return yPix;


# templateValues is a dictionary which contains all the values which get used in the templates
def calcStatisticsAroundPoint(templateValues, currentSatJson, algStr, nrDays):
    dateStr = templateValues["dateStr"]
    sat = templateValues["satellite"]
    alg = templateValues["algorithm"]

    print(templateValues["latStr"])

    lat = float(templateValues["latStr"])
    long = float(templateValues["longStr"])
    zoom = float(templateValues["zoomStr"])

    print(lat)
    print(long)

     # how many pixels do i move around if I increase the latitude by 1.0
    pixelPerlat = lat2yPixel(lat, zoom) - lat2yPixel(lat + 1.0, zoom)
    # how much changes the lat, when I move one pixel (note: one tile is 256 pixels wide)
    latPerPixel = (1.0 / pixelPerlat) / 256.0
    print("LatPerPix: ")
    print(latPerPixel)

    left = long - latPerPixel * 32.0
    top = lat - latPerPixel * 32.0

    right = long + latPerPixel * 32.0
    bottom = lat + latPerPixel * 32.0

    print ("LEFT: " + str(left) + "Right: " + str(right) + "TOP " + str(top) + "BOTTOM " + str(bottom))

    print(zoom)

    # regionList = [18.013458251953125, 46.92963428624288, 18.10821533203125, 46.9980510299792]
    regionList = [left, top,right, bottom]
    print(regionList)
    region = ee.Geometry.Rectangle(regionList)

    resList = getStatistics(dateStr, sat, alg, currentSatJson, algStr, region, nrDays)
    return json.dumps(resList)


def getDate(d, sat, alg, satJson, algStr, region, nrDays):
    desiredDate = ee.Date.parse("yyyy-MM-dd", d)

    dateRange = ee.DateRange(desiredDate.advance(-nrDays/2, 'day'), desiredDate.advance(nrDays/2, 'day'))

    # richtige imagecollection erzeugen, gleich filtern
    imageColl = ee.ImageCollection(bandDataImageStrings[sat]).filterDate(dateRange).filterBounds(region)
    imageColl = imageColl.sort('system:time_start', False)

    test = imageColl.aggregate_array("system:time_start")

    test2 = test.getInfo()

    result = []
    for x in range(0, len(test2)):
        date = datetime.datetime.fromtimestamp(int(test2[x] / 1000))
        print(date)
        result.append([0, test2[x]])

    print("end of getDate")
    return result

# templateValues is a dictionary which contains all the values which get used in the templates
# currentSatJson is a json object containing information about the current satellite
# algstr is the current algorithm
# nrDays for how many days to calclulate the statistics
def getDateAroundPoint(templateValues, currentSatJson, algStr, nrDays):
    dateStr = templateValues["dateStr"]
    sat = templateValues["satellite"]
    alg = templateValues["algorithm"]

    print(templateValues["latStr"])

    lat = float(templateValues["latStr"])
    long = float(templateValues["longStr"])
    zoom = float(templateValues["zoomStr"])

    print(lat)
    print(long)

    # how many pixels do i move around if I increase the latitude by 1.0
    pixelPerlat = lat2yPixel(lat, zoom) - lat2yPixel(lat + 1.0, zoom)
    # how much changes the lat, when I move one pixel (note: one tile is 256 pixels wide)
    latPerPixel = (1.0 / pixelPerlat) / 256.0
    print("LatPerPix: ")
    print(latPerPixel)

    left = long - latPerPixel * 2.0
    top = lat - latPerPixel * 2.0

    right = long + latPerPixel * 2.0
    bottom = lat + latPerPixel * 2.0

    print ("LEFT: " + str(left) + "Right: " + str(right) + "TOP " + str(top) + "BOTTOM " + str(bottom))

    print(zoom)

    # regionList = [18.013458251953125, 46.92963428624288, 18.10821533203125, 46.9980510299792]
    regionList = [left, top,right, bottom]
    print(regionList)
    region = ee.Geometry.Rectangle(regionList)

    resList = getDate(dateStr, sat, alg, currentSatJson, algStr, region, nrDays)
    return json.dumps(resList)
